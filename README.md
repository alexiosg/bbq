# Bayesian Binning into Quantiles #

Based on the paper (and matlab code) : "Obtaining Well Calibrated Probabilities Using Bayesian Binning" by Naeini, Cooper and Hauskrecht: http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4410090/

Includes required functions for estimation and prediction. 

To install use:

```
#!R

library(devtools)
install_bitbucket("alexiosg/bbq")
```

You should probably also make sure you have AUC installed (CRAN).